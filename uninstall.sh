#!/bin/bash
# uninstall.sh
# Copyright spoofy@twojapomoc.it

LEAGACY_GRAPHSRV="yes"
USER_ID="880"

if [ "$EUID" -ne 0 ]; then
  echo "[ERROR] please run as root! Aborting." 1>&2
  exit 1
fi

/bin/systemctl stop vaping.service
/bin/systemctl disable vaping.service
rm -rf /lib/systemd/system/vaping.service
/bin/systemctl daemon-reload

apt-get remove -y fping

yes |pip uninstall vaping
yes |pip uninstall vodka
if [ ${LEAGACY_GRAPHSRV} == "yes" ]; then
	yes |pip uninstall graphsrv==1.2.0
else
	yes |pip uninstall graphsrv
fi

userdel vaping
groupdel vaping

rm -rf /var/run/vaping
rm -rf /etc/vaping
