#!/bin/bash
# install.sh
# Copyright spoofy@twojapomoc.it

LEAGACY_GRAPHSRV="yes"
USER_ID="880"

if [ "$EUID" -ne 0 ]; then
  echo "[ERROR] please run as root! Aborting." 1>&2
  exit 1
fi

apt-get install -y fping

pip install vaping
pip install -U vodka
if [ ${LEAGACY_GRAPHSRV} == "yes" ]; then
	pip install graphsrv==1.2.0
else
	pip install -U graphsrv
fi

GROUP_ID="${USER_ID}"
groupadd -g ${GROUP_ID} vaping
useradd -u ${USER_ID} -g ${GROUP_ID} vaping -d /var/run/vaping

mkdir -v /var/run/vaping
mkdir -v /etc/vaping
chown -v vaping:vaping /var/run/vaping
cp -v source/config.yml /etc/vaping/config.yml
cp -v source/layout.yml /etc/vaping/layout.yml
cp -v source/vaping.service /lib/systemd/system/vaping.service
ln -v -s /etc/vaping/config.yml /var/run/vaping/config.yml
ln -v -s /etc/vaping/layout.yml /var/run/vaping/layout.yml
/bin/systemctl daemon-reload
/bin/systemctl enable vaping.service
/bin/systemctl start vaping.service
